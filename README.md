# Nie za krótkie wprowadzenie do systemu LaTeX2e

This is the Polish translation of "The not so short introduction to LaTeX2e" 
 by Tobias Oetiker et al. (ver 4.20).
(C) 1999, 2007, 2022 for the translation and extension by Marcin Serwin, Janusz Gołdasz,
 Ryszard Kubiak and Tomasz Przechlewski. 

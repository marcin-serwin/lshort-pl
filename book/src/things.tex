%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Contents: Things you need to know
% $Id$
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\chapter{Podstawy, które warto znać}
\begin{intro}
  W pierwszej części tego rozdziału przedstawimy krótko filozofię oraz
  historię systemu \LaTeXe. W części drugiej skoncentrujemy się na podstawowych
  elementach dokumentu \LaTeX{}owego. Po przeczytaniu tego rozdziału
  czytelnik powinien z grubsza wiedzieć, jak działa \LaTeX, co jest
  niezbędne do rozumienia materiału prezentowanego w następnych
  rozdziałach.
\end{intro}

\section{Nieco historii}
\subsection{\TeX}

\TeX{} jest programem komputerowym stworzonym przez \index{Knuth
  Donald E.}Donalda E.~Knutha~\cite{texbook}. Jest przeznaczony do
składu tekstów oraz wzorów matematycznych. Knuth rozpoczął pracę nad
\TeX{}em w 1977 roku, aby zbadać potencjał składu cyfrowego,
stosowanego wówczas na coraz szerszą skalę w poligrafii. Miał przede
wszystkim nadzieję, że uda się odwrócić tendencję do pogarszania się
jakości typograficznej, co uwidaczniało się w jego własnych książkach
i artykułach. W używanej obecnie postaci \TeX{} został udostępniony w
roku 1982, a niewielkie rozszerzenie, dotyczące ośmiobitowego
kodowania znaków oraz wielu języków, pojawiło się w roku 1989. \TeX{}
ma renomę programu nadzwyczaj stabilnego, pracującego na różnego
rodzaju sprzęcie oraz praktycznie wolnego od błędów. Numery wersji
\TeX{}a zbiegają do liczby $\pi$, a obecny wynosi $\num{3.14159265}$.

Słowo {\TeX} należy wymawiać ,,tech'', gdzie zgłoska ,,ch'' wymawiana
jest tak jak w niemieckim słowie ,,Ach''\footnote{W języku niemieckim
  istnieją tak naprawdę dwie wymowy dla ,,ch'' i można rozważyć, czy
  miękkie ,,ch'' ze słowa ,,Pech'' nie byłoby bardziej odpowiednie.
  Zapytany o to Knuth napisał na niemieckiej Wikipedii: \emph{Nie
    złoszczę się gdy ludzie wymawiają \TeX{} w swój ulubiony sposób
    \ldots{} a w Niemczech wielu używa miękkie ch, ponieważ X następuje
    po samogłosce e, a nie twardsze ch następujące po samogłosce a. W
    Rosji ,tex' jest bardzo pospolitym słowem wymawianym jako ,tyekh'.
    Osobiście uważam, że najbardziej poprawną wymowę można usłyszeć w
    Grecji, gdzie występuje bardziej szorstkie ch z ach oraz Loch.}} lub
w szkockim ,,Loch''. Zgłoska ,,ch'' pojawia się tu, ponieważ w
alfabecie greckim X oznacza literę ,,ch'' albo ,,chi''. \TeX{} jest
też pierwszą sylabą greckiego słowa technika. W tekstach ASCII \TeX{}
zapisujemy jako \texttt{TeX}.

\subsection{\LaTeX}

{\LaTeX} umożliwia autorom złożenie i wydrukowanie ich prac na
najwyższym poziomie typograficznym przy użyciu ustalonych,
profesjonalnych układów. Pierwszą wersję \LaTeX{}a opracował
\index{Lamport Leslie}Leslie Lamport~\cite{manual}. Do formatowania
dokumentu \LaTeX{} używa programu \TeX{}. W czasach obecnych \LaTeX{}
jest utrzymywany przez \index{The \LaTeX{} Project}The \LaTeX{}
Project.

%In 1994 the \LaTeX{} package was updated by the \index{LaTeX3@\LaTeX
%  3}\LaTeX 3 team, led by \index{Mittelbach, Frank}Frank Mittelbach,
%to include some long-requested improvements, and to re\-unify all the
%patched versions which had cropped up since the release of
%\index{LaTeX 2.09@\LaTeX{} 2.09}\LaTeX{} 2.09 some years earlier. To
%distinguish the new version from the old, it is called \index{LaTeX
%2e@\LaTeXe}\LaTeXe. This documentation deals with \LaTeXe. These days you
%might be hard pressed to find the venerable \LaTeX{} 2.09 installed
%anywhere.

Słowo \LaTeX{} należy wymawiać ,,lej-tech'' albo ,,la-tech''. W
tekstach ASCII \LaTeX{} zapisujemy jako \texttt{LaTeX}. \LaTeXe{}
wymawiamy ,,la-tech dwa i'', a w tekstach ASCII piszemy
\texttt{LaTeX2e}.

%Figure~\ref{components} above % on page \pageref{components}
%shows how \TeX{} and \LaTeXe{} work together. This figure is taken from
%\texttt{wots.tex} by Kees van der Laan.

%\begin{figure}[btp]
%\begin{lined}{0.8\textwidth}
%\begin{center}
%\input{kees.fig}
%\end{center}
%\end{lined}
%\caption{Components of a \TeX{} System.} \label{components}
%\end{figure}

\section{Podstawy}

\subsection{Autor, redaktor i zecer}

Aby wydać książkę, autor dostarcza maszynopis do wydawnictwa. W
wydawnictwie redaktor decyduje o układzie graficznym dokumentu
(szerokość szpalty, krój pisma, odstępy przed i po tytułach
rozdziałów itp.). Redaktor zapisuje swoje decyzje w maszynopisie, w
formie odpowiednich instrukcji, i przekazuje go zecerowi. Na
podstawie maszynopisu oraz instrukcji zecer wykonuje skład.

Redaktor-człowiek próbuje odgadnąć, co autor miał na myśli, gdy
zapisywał maszynopis. Wykorzystując swoje doświadczenie zawodowe,
ustala, które miejsca w maszynopisie oznaczają tytuły rozdziałów,
podrozdziałów, cytaty, przykłady, wzory matematyczne itd.

\LaTeX{} gra rolę redaktora i wykorzystuje \TeX{}a jako swojego
zecera. Jednakże \LaTeX{} jest ,,zaledwie'' programem komputerowym i
dlatego potrzebuje dodatkowej pomocy autora, który powinien
dostarczyć niezbędnych do składu informacji o strukturze logicznej
dokumentu. Informacje te autor zapisuje w pliku źródłowym dokumentu
jako ,,polecenia {\LaTeX}a''.

Jest to istotnie różne od podejścia
\wi{WYSIWYG}\footnote{ang.~\emph{\textenglish{What you see is what
      you get}} -- dostaniesz to, co widzisz.} podjętego przez nowoczesne
procesory tekstu, takie jak \emph{MS Word} czy \emph{LibreOffice}. W
takich aplikacjach autorzy ustalają układ interaktywnie podczas
wprowadzania tekstu do komputera. Na ekranie widzą oni jak będzie
wyglądała ostateczna pracy gdy zostanie wydrukowana.

Używając \LaTeX{}a, nie można na ogół oglądać dokumentu w jego
ostatecznej postaci podczas wprowadzania tekstu. Można natomiast
obejrzeć dokument na ekranie po przetworzeniu go \LaTeX{}em. Po
dokonaniu korekcji dokument taki można już wysłać do drukowania.

\subsection{Układ graficzny}

Projektowanie książek jest sztuką. Niedoświadczeni autorzy często
popełniają poważne błędy, zakładając, że zaprojektowanie układu
graficznego książki jest jedynie kwestią estetyki (jeżeli dokument
ładnie wygląda, to jest dobrze złożony). Jednak jako że dokumenty są
przeznaczone do czytania, a nie do wieszania na ścianie w galerii
sztuki, to o wiele większe znaczenie niż piękny wygląd ma łatwość
czytania i przyswajania tekstu. Przykłady:
\begin{itemize}
  \item rozmiar fontu oraz numerację nagłówków należy ustalić tak, by
        czytelnik mógł się szybko zorientować w strukturze dokumentu;
  \item szerokość szpalty powinna być na tyle wąska, by czytelnik nie musiał
        wytężać wzroku, wystarczająco jednak duża, aby tekst elegancko
        wypełniał stronę.
\end{itemize}

W systemach \wi{WYSIWYG} często powstają dokumenty przyjemne dla oka,
ale pozbawione struktury albo wykazujące brak konsekwencji w
strukturze. \LaTeX{} zapobiega powstawaniu takich błędów, zmuszając
autora do określenia \emph{logicznej} struktury dokumentu. Do
\LaTeX{}a należy dobór najodpowiedniejszego dla niej układu
graficznego.

\subsection{Zalety i wady}

Tematem często dyskutowanym, gdy użytkownicy programów typu
\wi{WYSIWYG} spotykają użytkowników \LaTeX{}a, są ,,\wi{zalety
  \LaTeX{}a} w porównaniu ze zwykłym procesorem tekstu'' albo na
odwrót. Najlepiej podczas takich dyskusji siedzieć cicho, gdyż często
wymykają się one spod kontroli. Czasami jednak nie ma ucieczki\ldots

\medskip\noindent Na wszelki wypadek trochę amunicji. Oto
najważniejsze zalety \LaTeX{}a w porównaniu ze zwykłymi procesorami
tekstu:

\begin{itemize}

  \item Profesjonalnie przygotowane układy, dzięki którym dokumenty wyglądają
        ,,jak z drukarni''.
  \item Wprowadzanie formuł matematycznych jest wspierane i możliwe w wygodny
        sposób.
  \item Do rozpoczęcia pracy wystarczy poznać zaledwie kilkanaście łatwych do
        zrozumienia instrukcji, określających strukturę logiczną dokumentu.
        Niemal nigdy nie trzeba zaprzątać sobie głowy formatowaniem
        dokumentu.
  \item Nawet skomplikowane elementy, takie jak: przypisy, odnośniki, spisy
        treści oraz spisy bibliograficzne mogą zostać łatwo wygenerowane.
  \item Wolne i darmowe pakiety znacząco poszerzające typograficzne
        możliwości \LaTeX{}a. Przykładowo istnieją pakiety umożliwiające
        wstawianie do dokumentów grafiki w formacie \PSi{} czy też
        przygotowanie spisów bibliograficznych według ściśle określonych
        reguł. Opis wielu z tych pakietów można znaleźć w podręczniku
        \companion.
  \item \LaTeX{} zachęca autorów do tworzenia dokumentów o dobrze określonej
        strukturze.
  \item \TeX, program formatujący używany przez \LaTeXe{}, jest dalece
        przenośny, wolny oraz bezpłatny. Dzięki temu można działać na
        praktycznie dowolnej platformie systemowo-sprzętowej.

        %
        % Add examples ...
        %
\end{itemize}

\medskip

\noindent \LaTeX{} ma także pewne wady, chociaż ciężko mi znaleźć
jakąkolwiek istotną. Jestem jednak pewien, że inne osoby wskażą ci
ich setki \texttt{;-)}

\begin{itemize}
  \item \LaTeX{} nie działa u tych, którzy zaprzedali swoje dusze\ldots
  \item Chociaż przez zmianę niektórych parametrów można dostosowywać
        predefiniowane układy graficzne do własnych potrzeb, to jednak
        zaprojektowanie całkowicie nowego układu jest trudne i
        czasochłonne\footnote{Plotki mówią, że jest to jeden z ważniejszych
          problemów, nad jakim pracują twórcy systemu \LaTeX
          3.}\index{LaTeX3@\LaTeX 3}.
  \item Trudno jest tworzyć dokumenty o nieokreślonej, bałaganiarskiej
        strukturze.
  \item Twój chomik może, pomimo obiecujących pierwszych kroków, nie być w
        stanie w pełni pojąć koncepcji znakowania logicznego.
\end{itemize}

\section{Pliki źródłowe \LaTeX{}a}

Plik źródłowy \LaTeX{}a to zwykły plik tekstowy. Na systemach
Unix/Linux pliki tekstowe występują bardzo często. Aby stworzyć taki
plik na Windowsie można wykorzystać Notatnik. Pliki takie zawierają
treść dokumentu oraz instrukcje dla \LaTeX{}a określające, jak tekst
ma zostać złożony. Jeśli pracujesz w \LaTeX{}owym IDE to będzie ono
zawierało program do tworzenia plików źródłowych \LaTeX{}a w formie
tekstowej.

\subsection{Odstępy}

Znaki ,,niedrukowalne'', takie jak odstępy (spacje) lub znaki
tabulacji, są przez \LaTeX{}a traktowane jednakowo -- jako
,,\wi{odstęp}''. \emph{Kolejno} po sobie występujące \wi{znaki
  niedrukowalne} \LaTeX{} traktuje jak \emph{pojedynczy} ,,odstęp''.
Znaki niedrukowalne znajdujące się na początku wiersza są prawie
zawsze ignorowane\index{odstęp!na początku wiersza}. Pojedynczy
koniec linii jest traktowany jak odstęp.

Pusty wiersz pomiędzy dwoma wierszami tekstu oznacza koniec akapitu.
\emph{Kolejno} występujące puste wiersze \LaTeX{} traktuje jak
\emph{jeden}. Przykładem może być poniższy tekst. Po prawej stronie
(w ramce) przedstawiono wynik składu, a po lewej -- zawartość pliku
źródłowego.

\begin{example}
Nie ma znaczenia,
czy między słowami jest
jedna czy     więcej spacji.

Pusty wiersz zakończył poprzedni
akapit.
\end{example}

\subsection{Znaki specjalne}

Poniższe symbole są znakami zarezerwowanymi\index{znaki
  zarezerwowane} -- w tym sensie, że albo mają dla \LaTeX{}a specjalne
znaczenie, albo nie są dostępne we wszystkich standardowych krojach
pisma. Użyte dosłownie w pliku źródłowym nie pojawią się na wydruku,
lecz spowodują, że \LaTeX{} zrobi coś niepożądanego.

\begin{code}
\verb.#  $  %  ^  &  _  {  }  ~  \ . %$
\end{code}

Jak wkrótce zobaczysz znaki te można umieścić w dokumencie pod
warunkiem, że w pliku źródłowym zostaną poprzedzone ukośnikiem
wstecznym.

\begin{example}
\# \$ \% \^{} \& \_ \{ \} \~{}
\textbackslash
\end{example}

Inne symbole oraz wiele innych może zostać wstawione przy użyciu
specjalnych poleceń w formułach matematycznych lub jako akcenty. Znak
ukośnika wstecznego \textbackslash{} \emph{nie} może zostać wstawiony
przez poprzedzenie go nim samym (\verb|\\|); ta sekwencja znaków jest
używana do łamania linii. Aby go wprowadzić użyj polecenia
\ci{textbackslash}.

\subsection{Polecenia \LaTeX{}a}

Polecenia\index{polecenia} \LaTeX{}a mogą wystąpić w dwóch
następujących odmianach:

\begin{itemize}
  \item Zaczynają się ukośnikiem wstecznym\index{ukośnik wsteczny} \verb|\|
        po którym następuje ich nazwa składająca się tylko z liter. Nazwy
        poleceń są zakończeniu przez spację, liczbę bądź inną ,,nie-literę''.
        Wielkość liter ma tutaj znaczenie.
  \item Składają się z ukośnika wstecznego i jednej ,,nie-litery''.
  \item Wiele poleceń posiada wersję z gwiazdką, uzyskiwaną przez dodanie na
        końcu jej nazwy asterysku (\texttt{*}).
\end{itemize}

%
% \\* doesn't comply !
%

%
% Can \3 be a valid command ? (jacoboni)
%
\label{znaki niedrukowalne} \LaTeX{} ignoruje znaki niedrukowalne
występujące po nazwie polecenia. Jeśli chcesz uzyskać odstęp po
komendzie\index{odstęp!po komendzie}, musisz wstawić po niej pusty
argument \verb|{}| i odstęp lub użyć specjalnych poleceń
wstawiających odstępy. Pusty argument \verb|{}| zapobiega
zignorowaniu przez {\LaTeX}a odstępu po nazwie polecenia.

\begin{example}
Nowi użytkownicy systemu \TeX
mogą zgubić odstęp po poleceniu.
Doświadczeni \TeX nicy
systemu \TeX{} wiedzą jak
poprawnie używać odstępów.
\end{example}

Niektóre instrukcje \LaTeX{}owe wymagają argumentów\index{argument}.
Podaje się je w nawiasach klamrowych\index{nawiasy klamrowe}
\verb|{ }| po nazwie komendy. Instrukcje mogą mieć także argumenty
opcjonalne\index{argument!opcjonalny}, podawane w nawiasach
kwadratowych\index{nawiasy kwadratowe} \verb|[ ]|.
\begin{code}
\verb|\|\textit{polecenie}\verb|[|\textit{argument opcjonalny}\verb|]{|\textit{argument}\verb|}|
\end{code}
Poniższe przykłady wykorzystują kilka poleceń {\LaTeX}owych. Nie
przejmuj się jeśli ich nie rozumiesz, zostaną opisane później.

\begin{example}
Możesz  na mnie \textsl{polegać}!
\end{example}
\begin{example}
Proszę, rozpocznij nową linię
dokładnie tutaj!\newline
Dziękuję.
\end{example}

\subsection{Komentarze}
\index{komentarz}

Po napotkaniu znaku \verb|%| {\LaTeX} ignoruje resztę bieżącego
wiersza, znak końca wiersza oraz znaki odstępu na początku
następnego.

Może być to używane do umieszczania dodatkowych notatek wewnątrz
plików źródłowych, które nie zostaną pokazane w wersji wydrukowanej.

\begin{example}
To jest % głupi
% Lepiej: pouczający <----
przykład: konstantyno%
              politańczy%
    kowianeczka
\end{example}

Znak \texttt{\%} może być również użyty do dzielenia bardzo długich
linii w pliku wejściowym, gdy niedozwolone jest użycie spacji lub
złamanie wiersza.

% TODO: Rozważyć
%% Tego nie tłumaczymy bo jest niepotrzebne i mylące
%% [ pojawia się pojęcie pakietu, otoczenia]:

W przypadku dłuższych komentarzy można użyć otoczenia \ei{comment} z
pakietu \pai{verbatim}. Wystarczy dodać linię
\verb|\usepackage{verbatim}| do preambuły twojego dokumentu -- jak to
zrobić opisane jest później -- by móc korzystać z tego polecenia.

\begin{example}
Oto następny
\begin{comment}
nieco głupi,
lecz pomocny
\end{comment}
przykład wstawiania 
komentarzy do twojego dokumentu.
\end{example}

Zwróć uwagę, że sposób ten nie zadziała w skomplikowanych
otoczeniach, takich jak np. tryb wprowadzania wzorów matematycznych.

\section{Struktura pliku źródłowego}
\label{sec:structure} Gdy \LaTeX{} przetwarza plik źródłowy, oczekuje
on, że posiada określoną strukturę\index{struktura}. W związku z tym
każdy plik musi rozpoczynać się od polecenia

\begin{code}
\verb|\documentclass{...}|
\end{code}
Instrukcja ta określa rodzaj tworzonego dokumentu. Po niej można
umieścić polecenia dotyczące stylu całego dokumentu oraz dołączyć
\wi{pakiet}y poszerzające możliwości \LaTeX{}a. Aby je załadować
należy użyć polecenia
\begin{code}
\verb|\usepackage{...}|
\end{code}

Gdy wszystkie style i pakiety zostały ustawione\footnote{Obszar
  pomiędzy poleceniami \texttt{\bs documentclass} i \texttt{\bs
    begin$\mathtt{\{}$document$\mathtt{\}}$} nazywa się
  \emph{preambułą}\index{preambuła}.}, rozpoczyna się ciało tekstu przy
użyciu polecenia

\begin{code}
\verb|\begin{document}|
\end{code}

Dalej znajduje się tekst dokumentu, wzbogacony o \LaTeX{}owe
polecenia. Na końcu dokumentu musi występować polecenie
\begin{code}
\verb|\end{document}|
\end{code}
które informuje \LaTeX{}a, że to koniec pracy. Wszystko co następuje
po tym poleceniu zostanie zignorowane przez \LaTeX{}a.

Rysunek~\ref{mini} pokazuje zawartość minimalnego dokumentu
  {\LaTeX}owego. Nieco bardziej skomplikowany \wi{plik źródłowy}
pokazany jest w rysunku~\ref{document}.

\begin{figure}[!bp]
  \begin{lined}{6cm}
    \begin{verbatim}
\documentclass{article}
\begin{document}
Małe jest piękne
\end{document}
\end{verbatim}
  \end{lined}
  \caption{Minimalny dokument \LaTeX{}owy.} \label{mini}
\end{figure}

\begin{figure}[!bp]
  \begin{lined}{10cm}
    \begin{verbatim}
\documentclass[a4paper,11pt]{article}
\usepackage{polski}
% Zdefiniuj tytuł
\author{H.~Partl}
\title{Minimalizm}
\begin{document}
% Generuje tytuł
\maketitle
% Wstawia spis treści
\tableofcontents
\section{Jakieś Interesujące słowa}
A tutaj rozpoczyna się nas cudowny artykuł.
\section{Żegnaj świecie}
\ldots{} a tutaj się kończy.
\end{document}
\end{verbatim}
  \end{lined}
  \caption[Przykład artykułu do czasopisma]{Przykład artykułu do
    czasopisma. Użyte w nim polecenia zostaną objaśnione
    w dalszej części.} \label{document}

\end{figure}

\section{Typowa sesja pracy z \LaTeX{}em}

Na pewno nie możesz się już doczekać, aby wypróbować drobny plik
źródłowy ze strony \pageref{mini}. Oto jak to zrobić: \LaTeX{} sam z
siebie nie posiada żadnego interfejsu graficznego lub wymyślnych
przycisków do kliknięcia. To po prostu program, który przetwarza twój
plik źródłowy. Niektóre instalacje \LaTeX{}a zawierają taki
interfejs, w których można znaleźć przycisk do kompilowania pliku
źródłowego. Na innych jedynym sposobem będzie wpisanie poleceń. Zwróć
uwagę, że poniższe instrukcje zakładają, że działająca instalacja
\LaTeX{}a znajduje się już na twoim komputerze\footnote{Będzie tak w
  przypadku dobrze zadbanych systemów Unixowych, a\ldots{} szanujące
  się osoby korzystają z Unixa, więc\ldots{} \texttt{;-)}}. Jeśli tak nie jest
to możesz chcieć wpierw obejrzeć dodatek~\ref{installinglatex} 
na stronie~\pageref{installinglatex}.

\begin{enumerate}
  \item Stwórz/Edytuj swój plik źródłowy \LaTeX{}. Plik ten musi być plikiem
        tekstowym. Na systemach unixowych wszystkie edytory stworzą dokładnie
        taki. Na Windowsie należy się upewnić, by przy zapisywaniu pliku
        wybrać format \emph{Plik Tesktowy}. Przy wybieraniu nazwy dla pliku
        upewnij się, że zawiera rozszerzenie \eei{.tex}.

  \item Otwórz terminal lub okno poleceń, przejdź do katalogu w którym
        zapisany jest twój plik (polecenie \texttt{cd} --
        \emph{\textenglish{change directory}}) i uruchom na nim \LaTeX{}a.
        Jeśli operacja się powiedzie, w katalogu pojawi się plik
        \texttt{.pdf}. Może być konieczne wielokrotne uruchomienie \LaTeX{}a
        by poprawnie wygenerować spis treści i wszystkie odnośniki. Jeśli w
        twoim kodzie jest błąd \LaTeX{} poinformuje cię o tym i przestanie
        przetwarzać plik. Naciśnij \texttt{ctrl-D} by powrócić do linii
        poleceń.
        \begin{lscommand}
          \verb+xelatex foo.tex+
        \end{lscommand}

\end{enumerate}

\section{Układ graficzny dokumentu}

\subsection {Klasy dokumentów}\label{sec:documentclass}

Na samym początku przetwarzania pliku źródłowego {\LaTeX} musi się
dowiedzieć, jakiego typu dokument autor chce uzyskać. Określone jest
to w instrukcji \ci{documentclass}:
\begin{lscommand}
  \ci{documentclass}\verb|[|\emph{opcje}\verb|]{|\emph{klasa}\verb|}|
\end{lscommand}
\noindent
gdzie \emph{klasa} oznacza typ dokumentu, który ma zostać utworzony.
W tabeli~\ref{documentclasses} zestawiono klasy dokumentów opisane w
niniejszym wprowadzeniu. Dystrybucja \LaTeXe{} dostarcza również inne
klasy, między innymi do pisania listów czy tworzenia slajdów.
Argument \emph{\wi{opcje}} dostosowuje zachowanie klasy dokumentu.
Opcje muszą być oddzielone przecinkami. Najczęstsze opcje
standardowych klas dokumentów są zestawione w tabeli~\ref{options}.

\begin{table}[!bp]
  \caption{Klasy dokumentów.} \label{documentclasses}
  \begin{lined}{\textwidth}
    \begin{description}

      \item [\normalfont\texttt{article}] dla artykułów w czasopismach naukowych,
            prezentacji, krótkich sprawozdań, dokumentacji programów, zaproszeń,
            \ldots \index{klasa!article}
      \item [\normalfont\texttt{proc}] klasa dla zbiorów artykułów z konferencji
            (\textenglish{proceedings}) opartych na klasie article.
            \index{klasa!proc}
      \item [\normalfont\texttt{minimal}] jest tak mała jak tylko się da. Ustawia
            tylko rozmiar strony oraz podstawowy font. Wykorzystywana głównie do
            wyłapywania błędów. \index{klasa!minimal}
      \item [\normalfont\texttt{report}] dla dłuższych sprawozdań zawierających
            kilka rozdziałów, krótkich książek, prac dyplomowych,
            \ldots\index{report!klasa}
      \item [\normalfont\texttt{book}] dla prawdziwych książek \index{klasa!book}
      \item [\normalfont\texttt{slides}] dla prezentacji. Klasa ta używa dużych
            liter bezszeryfowych. Możesz również rozważyć wybór nowszej klasy
            Beamer. \index{klasa!slides}
    \end{description}
  \end{lined}
\end{table}

\begin{table}[!bp]
  \caption{Opcje klas dokumentów.} \label{options}
  \begin{lined}{\textwidth}
    \begin{flushleft}
      \begin{description}
        \item[\normalfont\texttt{10pt}, \texttt{11pt}, \texttt{12pt}] \quad
          Ustawia rozmiar fontu dla tekstu zasadniczego dokumentu. Domyślną
          wartością jest 10 punktów.\index{rozmiar fontu}
          % TODO: Sprawdzić default
        \item[\normalfont\texttt{a4paper}, \texttt{letterpaper}, \ldots] \quad
          Definiuje format arkusza. Domyślną wartością jest
          \texttt{letterpaper}. Inne dopuszczalne wartości to:
          \texttt{a5paper}, \texttt{b5paper}, \texttt{executivepaper} i
          \texttt{legalpaper}. \index{format arkusza} \index{rozmiar kartki}
          \index{wymiary kartki} \index{A4}\index{letter (format arkusza)}
          \index{A5} \index{B5} \index{executive (format arkusza)}

        \item[\normalfont\texttt{fleqn}] \quad Składanie wyeksponowanych wzorów
          matematycznych od lewego marginesu zamiast domyślnego centrowania.

        \item[\normalfont\texttt{leqno}] \quad Umieszczanie numerów wzorów
          matematycznych na lewym marginesie zamiast domyślnie na prawym.

        \item[\normalfont\texttt{titlepage}, \texttt{notitlepage}] \quad Ustala
          czy nowa strona powinna zostać wstawiona po tytule dokumentu
          (\texttt{titlepage})\index{tytuł dokumentu} czy nie
          (\texttt{notitlepage}). Domyślnie klasa \texttt{article} nie
          rozpoczyna nowej strony, ale \texttt{report} i \texttt{book} już tak.
          \index{tytuł}

        \item[\normalfont\texttt{onecolumn}, \texttt{twocolumn}] \quad Ustawia czy
          tekst powinien być składany w jednej (\texttt{onecolumn}) czy dwóch
          kolumnach (\texttt{twocolumn}). \index{jedna kolumna} \index{dwie
            kolumny}

        \item[\normalfont\texttt{twoside, oneside}] \quad Ustawia czy wyjście
          powinno być złożone do druku jednostronnego (\texttt{oneside}) czy
          dwustronnego (\texttt{twoside}). Klasy \texttt{article} i
          \texttt{report} są domyślnie \wi{jednostronne}, a klasa \texttt{book}
          jest domyślnie dwustronna\index{dwustronne}. Zwróć uwagę, że ta opcja
          zmienia tylko styl twojego dokumentu. Opcja ta \emph{nie} poinstruuje
          twojej drukarki, że chcesz zrobić dwustronny wydruk.
        \item[\normalfont\texttt{landscape}] \quad Zmienia układ strony na
          poziomy.
        \item[\normalfont\texttt{openright, openany}] \quad Wybranie pierwszej
          opcji powoduje, że tytuły rozdziałów będą umieszczane tylko na
          stronach nieparzystych (pusta kartka zostanie dodana w razie
          potrzeby). W klasie \texttt{article} opcja nie ma znaczenia, gdyż w
          tej klasie nie jest zdefiniowane pojęcie rozdziału. W klasie
          \texttt{report} domyślną wartością jest \texttt{openany}, a w klasie
          book -- \texttt{openright}.
      \end{description}
    \end{flushleft}
  \end{lined}
\end{table}

Przykład: Plik źródłowy może się rozpoczynać od następującej
instrukcji
\begin{code}
\ci{documentclass}\verb|[11pt,twoside,a4paper]{article}|
\end{code}
która instruuje \LaTeX{}a by złożył dokument w klasie \emph{article},
fontem rozmiaru \emph{11 punktów}, przygotowany do wydruku po
\emph{dwóch} stronach na kartkach papieru formatu \emph{A4}.
\pagebreak[2]

\subsection{Pakiety}
\index{pakiet} Podczas pisania swoich dokumentów, może się okazać, że
w czasami potrzebujesz zrobić coś, czego podstawowy \LaTeX{} nie
potrafi. Jeśli chcesz włączyć grafikę\index{grafika}, \wi{kolorowy
  tekst} lub kod źródłowy do twojego dokumentu będziesz musiał
rozszerzyć możliwości \LaTeX{}a. Takie rozszerzenia nazywamy
pakietami. Dołącza się je poleceniem:
\begin{lscommand}
  \ci{usepackage}\verb|[|\emph{opcje}\verb|]{|\emph{pakiet}\verb|}|
\end{lscommand}
\noindent
gdzie \emph{pakiet\/} oznacza nazwę pakietu, a \emph{opcje} -- listę
rozdzielonych przecinkami opcji, które konfigurują zachowanie pakietu. Polecenie \ci{usepackage} umieszcza się zawsze w preambule. Jeśli nie pamiętasz co to zajrzyj do sekcji \ref{sec:structure}.

Część pakietów znajduje się w podstawowej dystrybucji \LaTeXe{}
(zobacz tabela \ref{packages}), inne są rozpowszechniane oddzielnie.
Jeśli \LaTeX{}a używamy w systemie, którym zarządza (dobry)
administrator, to informacja o dostępnych pakietach powinna się
znajdować w \guide. Podstawowym źródłem informacji o pakietach
\LaTeX{}a jest \companion. Zawiera on opis setek pakietów, a także
informuje, jak można pisać własne rozszerzenia \LaTeX{}a.

Nowoczesne dystrybucje \TeX{}a zawierają domyślnie dużą liczbę
pakietów. Jeśli pracujesz na systemie unixowym, możesz użyć komendy
\texttt{texdoc} by zobaczyć ich dokumentację. % TODO: Sprawdzić aktualność

\begin{table}[btp]
  \caption{Wybrane pakiety z podstawowej dystrybucji \LaTeX{}a} \label{packages}
  \begin{lined}{\textwidth}
    \begin{description}
      \item[\normalfont\pai{doc}]Służy do drukowania dokumentacji pakietów oraz
      innych części składowych \LaTeX{}a.\\ Opis znajduje się w pliku
      \texttt{doc.dtx}\footnote{Plik ten powinien być zainstalowany w twoim
        systemie. Aby otrzymać z niego plik DVI, wystarczy w katalogu z
        prawem do zapisu napisać \texttt{xelatex doc.dtx}. To samo stosuje się
        do innych pakietów z tej tabeli.} oraz w \companion.

      \item[\normalfont\pai{exscale}] Umożliwia skalowanie fontów
        matematycznych, tak by optycznie były zgodne z otaczającym tekstem,
        np. w tytułach rozdziałów. Opis w \texttt{ltexscale.dtx}.

        % TODO: Wywalić?
      \item[\normalfont\pai{fontenc}] Definiuje układ znaków, którego ma używać
        \LaTeX. Opis w \texttt{ltoutenc.dtx}.

      \item[\normalfont\pai{ifthen}] Umożliwia korzystanie z poleceń typu\\
        ,,jeśli\ldots{} to zrób\ldots{} w przeciwnym razie\ldots''\\ Opis w
        \texttt{ifthen.dtx} i \cite{companion}.

      \item[\normalfont\pai{latexsym}] Udostępnia \LaTeX{}owy font symboliczny.
        Opisany w \texttt{latexsym.dtx} oraz w \companion.

      \item[\normalfont\pai{makeidx}] Udostępnia polecenia do przygotowywania
        skorowidzów. Opis w punkcie \ref{sec:indexing} i \cite{companion}.

      \item[\normalfont\pai{syntonly}] Powoduje, że dokument jest przetwarzany
        bez składania go.

        % TODO: Wywalić?
      \item[\normalfont\pai{inputenc}] Definiuje układ znaków w pliku źródłowym,
        jak: ASCII, ISO Latin-1, ISO Latin-2, 437/850 IBM, Apple Macintosh,
        Next, ANSI-Windows albo układ zdefiniowany przez użytkownika. Opis w
        \texttt{inputenc.dtx}.
    \end{description}
  \end{lined}
\end{table}

\subsection{Style strony}

\LaTeX{} wspiera trzy predefiniowane kombinacje \wi{pagina
  górna}/\wi{pagina dolna} (\wi{nagłówek}/\wi{stopka}) -- tak zwane
style stron\index{styl strony}. Argument \emph{styl} polecenia
\index{styl strony!plain@\texttt{plain}} \index{plain@\texttt{plain}}
\index{styl strony!headings@\texttt{headings}}
\index{headings@\texttt{headings}} \index{styl
  strony!empty@\texttt{empty}} \index{empty@\texttt{empty}}
\begin{lscommand}
  \ci{pagestyle}\verb|{|\emph{styl}\verb|}|
\end{lscommand}
\noindent ustala którego z nich chcesz użyć. Tabela~\ref{pagestyle} zestawia
predefiniowane style stron.

\begin{table}[!hbp]
  \caption{Predefiniowane style stron w \LaTeX{}u.} \label{pagestyle}
  \begin{lined}{\textwidth}
    \begin{description}

      \item[\normalfont\texttt{plain}] pagina górna jest pusta, a pagina dolna
        zawiera wycentrowany numer strony. Ten styl jest domyślny;

      \item[\normalfont\texttt{headings}] pagina górna zawiera numer strony oraz
        tytuł, pagina dolna jest pusta (ten style jest użyty w niniejszym
        dokumencie);
      \item[\normalfont\texttt{empty}] pagina górna i dolna są puste.

    \end{description}
  \end{lined}
\end{table}

Możliwa jest także zmiana stylu bieżącej strony przy użyciu polecenia
\begin{lscommand}
  \ci{thispagestyle}\verb|{|\emph{style}\verb|}|
\end{lscommand}
Instrukcje stworzenia własnych stylów można znaleźć w \companion{} oraz w sekcji~\ref{sec:fancy} na stronie~\pageref{sec:fancy}.
%
% Pointer to the Fancy headings Package description !
%

\section{Pliki z którymi możesz się zetknąć}

Pracując z \LaTeX{}em, szybko zauważysz, że na dysku pojawia się
mnóstwo plików o różnych \wi{rozszerzenia}ch, o których nie masz
żadnego pojęcia. W poniższym wykazie objaśniono rozmaite \wi{typy
  plików}, z którymi możesz się zetknąć podczas pracy z \TeX{}em. Wykaz
ten nie pretenduje do kompletnego, dlatego napisz do nas, gdy
napotkasz jakieś nowe rozszerzenie, które uznasz za warte opisania.

\begin{description}

  \item[\eei{.tex}] Plik źródłowy z dokumentem w notacji \LaTeX{}a bądź
    zwykłego \TeX{}a. Można go kompilować programem \texttt{xelatex}.
  \item[\eei{.sty}] Pakiet makr \LaTeX{}owych. Plik tego typu można dołączać
    do dokumentu \LaTeX{}owego, używając do tego celu instrukcji
    \ci{usepackage}.
  \item[\eei{.dtx}] Documented \TeX{}. Udokumentowany \TeX{}. Jest to
    podstawowy format, w jakim dystrybuowane są style \LaTeX{}a. Skutkiem
    kompilacji pliku tego typu jest broszurka z udokumentowanymi makrami.
  \item[\eei{.ins}] Instalator dla plików \eei{.dtx}. Pobierając pakiet
    \LaTeX{}owy, otrzymasz na ogół pliki \eei{.dtx} i \eei{.ins}.
    Uruchomienie \LaTeX{}a na pliku \eei{.ins} powoduje rozpakowanie
    pliku .dtx.
  \item[\eei{.cls}] Plik z klasą \LaTeX{}a definiującą wygląd składanych w
    \LaTeX{}u dokumentów. Właśnie do tych plików odnosi się występująca
    na początku dokumentu instrukcja \ci{documentclass}.
  \item[\eei{.fd}] Definicja niektórych właściwości fontów \LaTeX{}a.
\end{description}
W~wyniku kompilacji dokumentu \LaTeX{}owego
powstają następujące pliki:

\begin{description}
  % TODO: Wyalić?
  % TODO: pdfLaTeX -> xelatex?
  \item[\eei{.dvi}] \emph{\textenglish{Device Independent File}\/} (plik
    niezależny od urządzenia), będący wynikiem kompilacji pliku
    źródłowego przez ,,tradycyjnego'' \LaTeX{}a. Możesz mu się przyjrzeć
    przy użyciu odpowiedniego programu bądź wysłać go do drukarki przy
    użyciu \texttt{dvips} lub podobnej aplikacji. Jeśli używasz
    \hologo{pdfLaTeX} to nie powinieneś zobaczyć takich plików.
  \item[\eei{.log}] Zawiera szczegółowy raport z tego, co się wydarzyło
    podczas kompilacji.
  \item[\eei{.toc}] Zawiera nagłówki rozdziałów i punktów dokumentu. Jest on
    czytany przez \LaTeX{}a w następnym przebiegu kompilacji, w celu
    wygenerowania spisu treści.
  \item[\eei{.lof}] Podobny do pliku \eei{.toc}, z tym że zawiera wykaz
    ilustracji.
  \item[\eei{.lot}] Tak samo, lecz dotyczy wykazu tabel.
  \item[\eei{.aux}] Inny plik pomocniczy, przenoszący informację z jednego
    przebiegu kompilacji do następnego. Jest używany między innymi do
    magazynowania informacji związanej z odsyłaczami występującymi w
    dokumencie.
  \item[\eei{.idx}] Jeśli dokument zawiera indeks (skorowidz), to w tym
    pliku \LaTeX{} zapisze wszystkie jego hasła. Do przetworzenia tego
    pliku służy program \texttt{makeindex}. Więcej o tworzeniu indeksów
    przeczytasz w punkcie \ref{sec:indexing} na stronie
    \pageref{sec:indexing}.
  \item[\eei{.ind}] Przetworzony plik .idx, gotowy do włączenia do dokumentu
    w następnym cyklu kompilacji.
  \item[\eei{.ilg}] Sprawozdanie z tego, co zrobił program
    \texttt{makeindex}.
\end{description}

% Package Info pointer
%
%

%
% Add Info on page-numbering, ...
% \pagenumbering

\section{Duże projekty}
Pracując nad dużym dokumentem, wygodnie jest podzielić plik źródłowy
na mniejsze części. W {\LaTeX}u mamy dwie instrukcje ułatwiające
pracę z tak podzielonymi dokumentami. Pierwszą z nich jest:

\begin{lscommand}
  \ci{include}\verb|{|\emph{nazwa pliku}\verb|}|
\end{lscommand}
\noindent Włącza ona do dokumentu zawartość pliku o nazwie \emph{nazwa
  pliku.tex}. Zwróć uwagę, że \LaTeX{} rozpoczyna nową stronę przed
przetworzeniem instrukcji z \emph{nazwa pliku.tex}.

Drugie poleceniem może być użyte w preambule. Pozwala ono by \LaTeX{}
przetworzył tylko niektóre z plików dołączonych poleceniem
\verb|\include|d.
\begin{lscommand}
  \ci{includeonly}\verb|{|\emph{nazwa pliku}\verb|,|\emph{nazwa pliku}%
  \verb|,|\ldots\verb|}|
\end{lscommand}
Gdy ta komenda jest obecna w preambule dokumentu, spośród instrukcji \ci{include} zostaną wykonane tylko te,
które dotyczą plików wymienionych w argumencie
\ci{includeonly}.

Polecenie \ci{include} rozpoczyna skład dołączanego tekstu od nowej
strony. W połączeniu z \ci{includeonly} w preambule, instrukcja
\ci{include} umożliwia przetwarzanie wybranych plików bez zmiany
miejsc łamania poszczególnych stron. Czasami jednak rozpoczynanie
składu od nowej strony nie jest pożądane. W takim przypadku można
użyć polecenia
\begin{lscommand}
  \ci{input}\verb|{|\emph{nazwa pliku}\verb|}|
\end{lscommand}
\noindent Wstawia ono zawartość podanego pliku już bez żadnych dodatkowych
efektów.

Aby szybko sprawdzić swój dokument pod kątem błędów możesz użyć
pakietu \pai{syntonly}. Sprawia on, że \LaTeX{} przetworzy twój
dokument sprawdzając tylko poprawność składni i użyte polecenia, ale
nie wyprodukuje żadnego pliku. \LaTeX{} działa szybciej w tym trybie,
więc może ci to zaoszczędzić nieco czasu. Używanie jest bardzo
proste:

\begin{verbatim}
\usepackage{syntonly}
\syntaxonly
\end{verbatim}
Gdy już zechcesz wyprodukować plik wystarczy zakomentować drugą
linijkę (dodając przed nią znak procent).

%

% Local Variables:
% TeX-master: "lshort2e"
% mode: latex
% mode: flyspell
% End:
